package Girocco::Util;

use strict;
use warnings;

use Girocco::Config;

BEGIN {
	use base qw(Exporter);
	our @EXPORT = qw(scrypt jailed_file
	                 lock_file unlock_file
	                 filedb_atomic_append filedb_atomic_edit
	                 valid_email valid_email_multi
			 valid_repo_url valid_web_url);
}


sub scrypt {
	my ($pwd) = @_;
	crypt($pwd, join ('', ('.', '/', 2..9, 'A'..'Z', 'a'..'z')[rand 64, rand 64]));
}

sub jailed_file {
	my ($filename) = @_;
	$Girocco::Config::chroot."/$filename";
}

sub lock_file {
	my ($path) = @_;

	$path .= '.lock';

	use Errno qw(EEXIST);
	use Fcntl qw(O_WRONLY O_CREAT O_EXCL);
	use IO::Handle;
	my $handle = new IO::Handle;

	unless (sysopen($handle, $path, O_WRONLY|O_CREAT|O_EXCL)) {
		my $cnt = 0;
		while (not sysopen($handle, $path, O_WRONLY|O_CREAT|O_EXCL)) {
			($! == EEXIST) or die "$path open failed: $!";
			($cnt++ < 16) or die "$path open failed: cannot open lockfile";
			sleep(1);
		}
	}
	# XXX: filedb-specific
	chmod 0664, $path or die "$path g+w failed: $!";

	$handle;
}

sub unlock_file {
	my ($path) = @_;

	rename "$path.lock", $path or die "$path unlock failed: $!";
}

sub filedb_atomic_append {
	my ($file, $line) = @_;
	my $id = 65536;

	open my $src, $file or die "$file open for reading failed: $!";
	my $dst = lock_file($file);

	while (<$src>) {
		my $aid = (split /:/)[2];
		$id = $aid + 1 if ($aid >= $id);

		print $dst $_ or die "$file(l) write failed: $!";
	}

	$line =~ s/\\i/$id/g;
	print $dst "$line\n" or die "$file(l) write failed: $!";

	close $dst or die "$file(l) close failed: $!";
	close $src;

	unlock_file($file);

	$id;
}

sub filedb_atomic_edit {
	my ($file, $fn) = @_;

	open my $src, $file or die "$file open for reading failed: $!";
	my $dst = lock_file($file);

	while (<$src>) {
		print $dst $fn->($_) or die "$file(l) write failed: $!";
	}

	close $dst or die "$file(l) close failed: $!";
	close $src;

	unlock_file($file);
}

sub valid_email {
	$_ = $_[0];
	/^[a-zA-Z0-9+._-]+@[a-zA-Z0-9-.]+$/;
}
sub valid_email_multi {
	$_ = $_[0];
	# More relaxed, we just want to avoid too dangerous characters.
	/^[a-zA-Z0-9+._, @-]+$/;
}
sub valid_web_url {
	$_ = $_[0];
	/^http:\/\/[a-zA-Z0-9-.]+(\/[_\%a-zA-Z0-9.\/~:-]*)?(#[a-zA-Z0-9._-]+)?$/;
}
sub valid_repo_url {
	$_ = $_[0];
	/^(http|git|svn|darcs|bzr):\/\/[a-zA-Z0-9-.:]+(\/[_\%a-zA-Z0-9.\/~-]*)?$/;
}


1;
