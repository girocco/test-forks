/* Girocco JS */


/* Hide/show various bits depending on mirror/push
 * mode in regproj.cgi: */

var tr_display;
function mirror_push__click(ev, which) {
	if (!ev) var ev = window.event;
	if (which == 'mirror') {
		document.getElementById('mirror_url').style.display = tr_display;
	} else {
		document.getElementById('mirror_url').style.display = 'none';
	}
}
function mirror_push_prepare(mr, pr) {
	if (!mr || !pr)
		return;
	mr.onclick = function(e) { mirror_push__click(e, 'mirror') };
	pr.onclick = function(e) { mirror_push__click(e, 'push') };
	tr_display = document.getElementById('mirror_url').style.display;
}
window.addEvent('domready', function() {
	mirror_push_prepare(document.getElementById('mirror_radio'),
			    document.getElementById('push_radio'));
});
