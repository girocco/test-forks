#!/bin/bash
#
# Invoked from taskd/taskd.pl

. @basedir@/shlib.sh

set -e

projdir="$1"
proj="${projdir%.git}"

cd "$cfg_reporoot/$projdir"
trap "echo '@OVER@'; touch .clone_failed" EXIT
url="$(config_get baseurl)"

if [ "$cfg_project_owners" = "source" ]; then
	config_set owner "$(stat -c %U "$url" 2>/dev/null)"
fi

mail="$(config_get owner)"

# Initial mirror
echo "Initiating mirroring..."
case "$url" in
	svn://*)
		GIT_DIR=. git svn init -s "$url"
		GIT_DIR=. git svn fetch
		# Neat Trick suggested by Miklos Vajna
		GIT_DIR=. git config remote.origin.url .
		GIT_DIR=. git config remote.origin.fetch +refs/remotes/heads/*:refs/heads/*
		GIT_DIR=. git config remote.origin.fetch +refs/remotes/trunk:refs/heads/master
		GIT_DIR=. git config remote.origin.fetch +refs/remotes/tags/*:refs/tags/*
		GIT_DIR=. git fetch
		;;
	darcs://*)
		httpurl="${url/darcs:\/\//http://}"
		"$cfg_basedir"/bin/darcs-fast-export --export-marks=$(pwd)/dfe-marks "$httpurl" | \
			git fast-import --export-marks=$(pwd)/gfi-marks
		# This is here because by default only the exit code of
		# git fast-import is checked
		[ ${PIPESTATUS[0]} = 0 -a ${PIPESTATUS[1]} = 0 ]
		;;
	bzr://*)
		# we just remove bzr:// here, a typical bzr url is just
		# "lp:foo"
		bzrurl="${url#bzr://}"
		bzr fast-export --export-marks=$(pwd)/bfe-marks "$bzrurl" | \
			git fast-import --export-marks=$(pwd)/gfi-marks
		[ ${PIPESTATUS[0]} = 0 -a ${PIPESTATUS[1]} = 0 ]
		;;
	*)
		git remote rm origin >/dev/null 2>&1 || :
		git remote add --mirror origin "$url"
		git remote update
		git remote prune origin
		;;
esac

# The rest
echo "Final touches..."
git update-server-info
trap "" EXIT
mail -s "[$cfg_name] $proj clone completed" "$mail,$cfg_admin" <<EOT
Congratulations! The clone of project $proj just completed.

	* Source URL: $url
	* GitWeb interface: $cfg_gitweburl/$projdir
	* Project settings: $cfg_webadmurl/editproj.cgi?name=$proj

Have a lot of fun.
EOT

echo "Mirroring finished successfuly!"
rm .clone_in_progress
echo "@OVER@"
