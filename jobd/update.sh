#!/bin/bash

. @basedir@/shlib.sh

# darcs fast-export | git fast-import with error handling
git_darcs_fetch() {
	"$cfg_basedir"/bin/darcs-fast-export --export-marks=$(pwd)/dfe-marks --import-marks=$(pwd)/dfe-marks "$1" | \
		git fast-import --export-marks=$(pwd)/gfi-marks --import-marks=$(pwd)/gfi-marks
	[ ${PIPESTATUS[0]} = 0 -a ${PIPESTATUS[1]} = 0 ]
	return $?
}

# bzr fast-export | git fast-import with error handling
git_bzr_fetch() {
	bzr fast-export --export-marks=$(pwd)/dfe-marks --import-marks=$(pwd)/dfe-marks "$1" | \
		git fast-import --export-marks=$(pwd)/gfi-marks --import-marks=$(pwd)/gfi-marks
	[ ${PIPESTATUS[0]} = 0 -a ${PIPESTATUS[1]} = 0 ]
	return $?
}
set -e

proj="$1"
cd "$cfg_reporoot/$proj.git"

if check_interval lastrefresh $cfg_min_mirror_interval; then
	progress "= [$proj] update skip (last at $(config_get lastrefresh))"
	exit 0
fi
progress "+ [$proj] update (`date`)"

bang_setup
bang_once=1
bang_action="update"

url="$(config_get baseurl)"
mail="$(config_get owner)"

bang git for-each-ref --format '%(refname)	%(objectname)' >.refs-before

case "$url" in
	svn://*)
		GIT_DIR=. bang git svn fetch
		GIT_DIR=. bang git fetch
		;;
	darcs://*)
		httpurl="${url/darcs:\/\//http://}"
		bang git_darcs_fetch "$httpurl"
		;;
	bzr://*)
		bzrurl="${url#bzr://}"
		bang git_bzr_fetch "$bzrurl"
		;;
	*)
		[ "$url" = "$(config_get remote.origin.url)" ] || bang config_set_raw remote.origin.url "$url"
		bang git remote update
		bang git remote prune origin
		;;
esac

bang git update-server-info
bang config_set lastrefresh "$(date -R)"

# Look at which refs changed and trigger ref-change for these
bang git for-each-ref --format '%(refname)	%(objectname)' >.refs-after
sockpath="$cfg_chroot/etc/taskd.socket"
if [ -S "$sockpath" ] && ! cmp -s .refs-before .refs-after; then
	join -j 1 .refs-before .refs-after |
		while read ref old new; do
			[ "$old" != "$new" ] || continue
			echo "ref-change -1 $proj $old $new $ref" | nc.openbsd -w 1 -U "$sockpath"
		done
	join -j 1 -v 1 .refs-before .refs-after |
		while read ref old; do
			echo "ref-change -1 $proj $old 0000000000000000000000000000000000000000 $ref" | nc.openbsd -w 1 -U "$sockpath"
		done
	join -j 1 -v 2 .refs-before .refs-after |
		while read ref new; do
			echo "ref-change -1 $proj 0000000000000000000000000000000000000000 $new $ref" | nc.openbsd -w 1 -U "$sockpath"
		done
fi

rm -f .refs-before .refs-after

if [ -e .banged ]; then
	echo "$proj update succeeded - failure recovery" | mail -s "[$cfg_name] $proj update succeeded" "$mail,$cfg_admin"
	rm .banged
fi

progress "- [$proj] update (`date`)"
